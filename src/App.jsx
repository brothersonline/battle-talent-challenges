import React, {Suspense } from "react";
import './Components/i18n';
import DefaultView from "./Components/Views/DefaultView";
import ReactGA from "react-ga4";

const trackingId = "G-SLSDWGYZ70"; // Replace with your Google Analytics tracking ID
ReactGA.initialize(trackingId);

function App() {
  ReactGA.pageview(window.location.pathname + window.location.search);

  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        <DefaultView />
      </Suspense>
    </div>
  );
}

export default App;
