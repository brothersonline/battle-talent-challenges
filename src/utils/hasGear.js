export const hasGear = (gearList, item) => {
  for (var i = 0; i < gearList.length; i++) {
    if (gearList[i].value.id === item) {
      return true;
    }
  }
  return false;
};
