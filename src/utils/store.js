import React, { useState } from 'react';

export const StoreContext = React.createContext(null);

export default ({ children }) => {
  const [showAllClasses, setShowAllClasses] = useState(false);

  const store = {
    showAllClasses,
    setShowAllClasses,
  };

  // eslint-disable-next-line react/jsx-filename-extension
  return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>;
};
