export const getStrengthPoints = () => {
  const strengthPoints = [];
  for (let i = 0; i < 14; i++) {
    strengthPoints.push(i+1);
  }
  return strengthPoints;
};

export const getWillpowerPoints = () => {
  const willpowerPoints = [];
  for (let i = 0; i < 21; i++) {
    willpowerPoints.push(i);
  }
  return willpowerPoints;
};
