import React from 'react';
import {useTranslation} from "react-i18next";
import rawChallenges from "../challenges.json";
import "./AllChallenges.css";
import styles from "./AllChallenges.module.css";

const createIdFromName = (name) => name.toLowerCase().replace(/ /g,"_");
const reformatChallenges = () => rawChallenges.map((challenge) => ({id: createIdFromName(challenge.name), checked: false, ...challenge}));

const getChallenges = () => {
  // let challenges = reformatChallenges();
  // const savedChallenges = JSON.parse(localStorage.getItem('battle_talent_challenges') || null) || [];

  // savedChallenges.forEach((savedChallenge) => {
  //   const index = challenges.findIndex(element => element.id === savedChallenge.id);
  //   challenges[index] = savedChallenge;
  // });
  return reformatChallenges();
};

function AllChallenges() {
  const { t } = useTranslation();
  const challenges = getChallenges();

  return (
    <section className="points text-center all-classes-container">
      <div className="container">
        <div className="row text-center animated fadeInUp">
          <div className={`col-md-12 ${styles.allChallenges}`}>
            <h1>{t('all_classes')}</h1>
            {challenges.map((challenge) => {
              // console.log(challenge);
              return (
                <div className={"class-row"} key={"name" + challenge.name}>
                  <div className={styles.classRowChallengeInfo}>
                    <h2>{challenge.name}</h2>
                    <p className={"description"}>{t(`${challenge.description}`)}</p>
                    {challenge.requirements && (
                    <div className={styles.requirementBtnContainer}>
                      {challenge.requirements.map((requirement) => {
                        //console.log(requirement);
                        return(
                          <a rel="noopener noreferrer" href={requirement.link} className={styles.requirementBtn} target="_blank">{t(`${requirement.name}`)}</a>
                        );}
                      )}
                    </div>
                    )}
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </section>
  );
}

export default AllChallenges;
