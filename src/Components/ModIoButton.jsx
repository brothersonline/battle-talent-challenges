import React from 'react';
import styles from './ModIoButton.module.css';

function ModIoBtn() {
  return <div className={styles.footer}>
    <a rel="noopener noreferrer" href={"https://battletalent.mod.io"} className={`btn ${styles.modIoButton}`} target="_blank">mod.io</a>
  </div>;
}

export default ModIoBtn;
