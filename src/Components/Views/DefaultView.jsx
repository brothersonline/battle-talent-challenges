import React, {useContext} from 'react';
import RandomChallenge from "../RandomChallenge";
import AllChallenges from "../AllChallenges";
import AllAchievements from "../AllAchievements";
import {StoreContext} from "../../utils/store";
import ModIoButton from "../ModIoButton";

function DefaultView() {
  const {showAllClasses} = useContext(StoreContext);

  return (
    <>
      <RandomChallenge />
      {showAllClasses === true && <AllChallenges />}
      <AllAchievements />
      <ModIoButton />
    </>
  );
}

export default DefaultView;
