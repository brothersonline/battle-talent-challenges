import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationsEN from '../Translations/en.json';

const resources = {
  en: {
    translation: translationsEN
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "nl",
    fallbackLng: "en",
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;
