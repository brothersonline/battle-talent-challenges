import React, {useContext, useState} from 'react';
import rawChallenges from "../challenges.json";
import "./RandomChallenge.css";
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";
import styles from "./RandomChallenge.module.css";

const createIdFromName = (name) => name.toLowerCase().replace(/ /g,"_");
const reformatChallenges = () => rawChallenges.map((challenge) => ({id: createIdFromName(challenge.name), checked: false, ...challenge}));

const getChallenges = () => {
  // let challenges = reformatChallenges();
  // const savedChallenges = JSON.parse(localStorage.getItem('battle_talent_challenges') || null) || [];
  //
  // savedChallenges.forEach((savedChallenge) => {
  //   const index = challenges.findIndex(element => element.id === savedChallenge.id);
  //   challenges[index] = savedChallenge;
  // });
  return reformatChallenges();
};

function RandomChallenge() {
  const { t } = useTranslation();
  const {showAllClasses, setShowAllClasses} = useContext(StoreContext);
  const [currentChallenge, setCurrentChallenge] = useState(null);
  const [isRandomizing, setIsRandomizing] = useState(false);
  const challenges = getChallenges();

  const setShowAllCLasses = (answer) => {
    setShowAllClasses(answer);
  }

  const setNewChallenge = () => {
    setIsRandomizing(true);

    const loopTime = 50
    loopThroughNames(loopTime)

    let i = 0;

    function loopThroughNames(increasingTime) {
      setTimeout(function() {
        const challenge = challenges[Math.floor(Math.random() * challenges.length)];
        setCurrentChallenge(challenge);
        i++;
        if (i < 10) {
          loopThroughNames(increasingTime);
          return;
        }
        setIsRandomizing(false);
      }, loopTime)
    }
  }

  return (
      <section className="text-center">
        <button className="btn" disabled={isRandomizing} onClick={() => setNewChallenge()}>{t('roll_class')}</button>
        {showAllClasses && <button className="btn" onClick={() => setShowAllCLasses(false)}>{t('hide_all_classes')}</button>}
        {!showAllClasses && <button className="btn" onClick={() => setShowAllCLasses(true)}>{t('show_all_classes')}</button>}
        {currentChallenge && (<div className={"random-challenge-container"}>
          <h2>{currentChallenge.name}</h2>
          <p>{isRandomizing ? "..." : currentChallenge.description}</p>
          {!isRandomizing && currentChallenge.requirements && (
            <div className={styles.requirementBtnContainer}>
              {currentChallenge.requirements.map((requirement) => {
                //console.log(requirement);
                return(
                  <a rel="noopener noreferrer" href={requirement.link} className={styles.requirementBtn} target="_blank">{t(`${requirement.name}`)}</a>
                );}
              )}
            </div>
          )}
        </div>)}
      </section>
  );
}

export default RandomChallenge;
