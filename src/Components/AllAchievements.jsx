import React, {useState} from 'react';
import {useTranslation} from "react-i18next";
import rawAchievements from "../achievements.json";
import "./AllAchievements.css";

const createIdFromName = (name) => name.toLowerCase().replace(/ /g,"_");
const reformatChallenges = () => rawAchievements.map((challenge) => ({id: createIdFromName(challenge.name), checked: false, ...challenge}));

const getChallenges = () => {
  let achievements = reformatChallenges();
  const savedChallenges = JSON.parse(localStorage.getItem('battle_talent_achievements') || null) || [];

  savedChallenges.forEach((savedChallenge) => {
    const index = achievements.findIndex(element => element.id === savedChallenge.id);
    achievements[index] = savedChallenge;
  });
  return achievements;
};

function AllChallenges() {
  const { t } = useTranslation();
  const [achievements, setChallenges] = useState(getChallenges());

  const setCheckboxCheck = (e) => {
    // console.log(e.target.checked)
    // console.log(e.target.id);

    const index = achievements.findIndex(element => element.id === e.target.id);

    let clone = [...achievements]
    clone[index].checked = e.target.checked;
    localStorage.setItem('battle_talent_achievements', JSON.stringify(clone));
    setChallenges(clone);
  }

  return (
    <section className="points text-center">
      <div className="container">
        <div className="row text-center animated fadeInUp">
          <div className="col-md-12 all-achievements">
            <h1>{t('achievements')}</h1>
            {achievements.map((challenge) => {
              return (
                <div className={"class-row"} key={"name" + challenge.name}>
                  <div className={"class-row-challenge-info"}>
                    <h2>{challenge.name}</h2>
                    <p className={"description"}>{t(`${challenge.description}`)}</p>
                  </div>
                  <input className="checkbox" type="checkbox" name="subscribe" id={challenge.id} checked={challenge.checked}	onChange={(e) => setCheckboxCheck(e)} />
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </section>
  );
}

export default AllChallenges;

